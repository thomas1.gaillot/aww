import AwwIcon from "@/app/components/icon/AwwIcon";
import SearchBar from "@/app/components/icon/SearchBar";

export default function HeadBar(){
    return <aside className={'px-32 py-4 w-screen flex gap-6 justify-between'}>
        <AwwIcon className={'w-[40px] h-[40px] hover:cursor-pointer'}/>
        <SearchBar/>
        <div className={"flex gap-4 items-center"}>
            <button className={'whitespace-nowrap text-xs font-semibold text-zinc-900 '}>Log In</button>
            <button className={'whitespace-nowrap text-xs font-semibold text-zinc-900 '}>Sign Up</button>
        </div>
        <div className={"flex gap-4 items-center"}>
            <button className={"text-sm font-semibold whitespace-nowrap px-8 rounded py-3 text-white bg-zinc-800 hover:bg-zinc-700"}>Be Pro</button>
            <button className={"text-sm font-semibold  whitespace-nowrap px-8 rounded py-3 border border-zinc-800  hover:bg-zinc-700 hover:text-white"}>Submit a News</button>
        </div>
    </aside>
}