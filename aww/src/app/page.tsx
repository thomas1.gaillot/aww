import Image from 'next/image'
import HeadBar from "@/app/headbar/page";

export default function Home() {
    return (<>
            <HeadBar/>
            <main className="flex min-h-screen flex-col items-center px-16 py-2">
                <HugeTitle />
                <ScrollingLargeText />
            </main>
        </>
    )
}

const HugeTitle  = () => <p className={"uppercase text-[#73C82C] font-bold whitespace-nowrap text-[140px]"}>GREEN OF THE DAY</p>

const ScrollingLargeText = () =>
    <ul className={"w-full animate-infinite-scroll text-zinc-800 text-2xl flex whitespace-nowrap gap-2  translate-x-full horizontal-scroll-animation"}>
        <li className={''}>Sep 7, 2023</li>
        <li className={''}>-</li>
        <li className={'font-bold'}>EACOP</li>
        <li className={''}>-</li>
        <li className={''}>Banks refused to support the project</li>
        <li className={''}>-</li>
        <li className={'font-bold'}>Green news of the day</li>
        <li className={''}>-</li>
        <li className={''}>Sep 7, 2023</li>
        <li className={''}>-</li>
        <li className={'font-bold'}>EACOP</li>
        <li className={''}>-</li>
        <li className={''}>Banks refused to support the project</li>
        <li className={''}>-</li>
        <li className={'font-bold'}>Green news of the day</li>
        <li className={''}>-</li>

    </ul>
