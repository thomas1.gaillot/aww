import {MagnifyingGlassOutlined} from "@/app/components/icon/MagnifyingGlass";
import {ChevronDownOutlined} from "@/app/components/icon/ChevronDown";

export default function SearchBar() {
    return <div className={'flex w-full'}>
        <div
            className={'w-full px-8 py-2.5 justify-between flex rounded-l-lg bg-zinc-400 text-zinc-400 bg-opacity-30 hover:bg-opacity-50 '}>
            <div className={"flex gap-4"}>
                <MagnifyingGlassOutlined className={'w-6 h-6'}/>
                <p className={'hover:cursor-text'}>Search for a good news</p>
            </div>
        </div>
        <div className={"flex gap-8 px-4 text-zinc-800 items-center rounded-r-lg hover:cursor-pointer bg-zinc-400 bg-opacity-30  hover:bg-opacity-70"}>
            <p className={"text-sm font-semibold"}>Environment</p>
            <ChevronDownOutlined className={'w-5 h-5'}/>
        </div>
    </div>
}